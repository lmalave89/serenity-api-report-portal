<h1>Automation API project & Report Portal Integration</h1>

<p>This project was designed to do like line base to create new projects that have like objective the automation of APIs, the technologies implemented in this project are:</p>

<h2><b>Architecture</b></h2>

<ul>
    <li><b>Program language: </b>Java</li>
    <li><b>Automation pattern design: </b>Screenplay</li>
    <li><b>Build automation tool: </b>Gradle</li>
    <li><b>Tracking Report: </b>Report Portal</li>
</ul>

<h2><b>Preconditions:</b></h2>

<ul>
    <li>Installed Java 8</li>
    <li>Installed Gradle</li>
    <li>Installed Nodejs</li>
    <li>Installed Docker</li>
    <li>Installed Git</li>
    <li>Installed Report Portal locally
        <ul>
            <li>Depending of the OS, open cmd or terminal</li>
            <li>Execute the command: <code>curl https://raw.githubusercontent.com/reportportal/reportportal/master/docker-compose.yml -o docker-compose.yml</code></li>
            <li>Execute the command: <code>docker-compose -p reportportal up -d --force-recreate</code></li>
            <li>Open in the browser the next URL: http://localhost:8080/ui/</li>
            <li>By default the admin user is: superadmin</li>
            <li>By default the password user is: erebus</li>
            <li>By default, the framework is using the port 8080 and 8081(it's mandatory to have those ports free - it's mandatory available)</li>
            <li>Be sure to change the password for security reasons.</li>
        </ul>
    </li>    
</ul>

<h2><b>Steps to execution:</b></h2>

<ul>
    <li>Clone the repository in a folder with the next command on your cmd or terminal: <code>git clone https://gitlab.com/lmalave89/serenity-api-report-portal.git</code></li>
    <li>Open the project in the IDE of your preference</li>
    <li>Depending of the IDE, maybe you should import the dependencies manually or synchronize manually</li>
    <li>In your terminal execute the next command to install the json-server: <code>npm -g i json-server</code></li>
    <li>Go to the root project. and in this location open a new cmd or terminal</li>
    <li>Execute the next command to start the server: <code>json-server --watch db.json</code></li>
    <li>The APIs for test in this project will be available while the JSON server is up</li>
    <li>For associate the project with your tracking tool "Report Portal", you need to do this steps:
        <ul>
            <li>Do login with superadmin user</li>
            <li>Go to profile user</li>
            <li>Copy the access token</li>
            <li>Paste the access token value in the "rp_uuid" field on the gradle.properties file in the project</li>
            <li>In the Report Portal go to the "administrative" section, on the administrator profile section</li>
            <li>Click in the "Add New Project" button</li>
            <li>Fill the field with the "automation_api_serenity" value</li>
            <li>In case that you would like to change the name of the project test in your report portal, you need to update the value of the field "rp_project" in the gradle.     properties file</li>     
        </ul>
    </li>      
    <li>To execute the automation,should execute the next command on the root of the project using cmd or terminal: <code>gradle clean aggregate</code></li>
    <li>By default executing the above command, the project will be executed in the QA environment and mode DEFAULT in the report portal tool</li>
    <li>If like to modify the default values with the next parameters:
        <ul>
            <li><code>-Denv=ENV:QA</code> for select QA environment ("It is the default value")</li>
            <li><code>-Denv=ENV:STG</code> for select STG environment</li>
            <li><code>-Denv=ENV:UAT</code> for select UAT environment</li>
            <li><code>-Denv=ENV:PROD</code> for select PROD environment</li>
            <li>Only can select one environment for execution</li>
            <li><code>-mode=DEBUG</code> for select mode DEBUG in the Report Portal ("It is the default value")</li>
            <li><code>-mode=DEFAULT</code> for select mode DEFAULT in the Report Portal</li>
            <li>Only can select one mode for execution</li>
            <li>Example with all parameters <code>gradle clean test aggregate -Denv=ENV:UAT -Denv=DEFAULT</code></li>
        </ul>
    </li>
    <li>For visualize the report generate for the automation framework you should go the the "target/site/serenity" folder generated after the execution in the root of the project and open the index.html file</li>
</ul>
