package exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class PostExceptions extends SerenityManagedException {

    public static final String FAILED_REFERENCE = "Failed for execute the POST method";

    public PostExceptions(String message, Throwable testErrorException) {
        super(message + FAILED_REFERENCE, testErrorException);
    }

}
