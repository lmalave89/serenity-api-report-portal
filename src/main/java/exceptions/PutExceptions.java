package exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class PutExceptions extends SerenityManagedException {

    public static final String FAILED_REFERENCE = "Failed for execute the PUT method";

    public PutExceptions(String message, Throwable testErrorException) {
        super(message + FAILED_REFERENCE, testErrorException);
    }
}
