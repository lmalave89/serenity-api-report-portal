package exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class DeleteExceptions  extends SerenityManagedException {

    public static final String FAILED_REFERENCE = "Failed for execute the Delete method";

    public DeleteExceptions(String message, Throwable testErrorException) {
        super(message + FAILED_REFERENCE, testErrorException);
    }
}
