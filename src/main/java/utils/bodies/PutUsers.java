package utils.bodies;

import com.google.gson.JsonObject;

public class PutUsers {

    public static String getData(){
        JsonObject data = new JsonObject();
        data.addProperty("name", "Lorena");
        data.addProperty("lastname", "Pastrana");
        data.addProperty("age", 29);
        return data.toString();
    }
}
