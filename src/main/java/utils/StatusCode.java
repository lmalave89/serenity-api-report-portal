package utils;

public enum StatusCode {

    CODE_200  (200),
    CODE_201  (201),
    CODE_404  (404),
    CODE_500  (500);


    private final int status;

    private StatusCode(int status) {
        this.status = status;
    }

    public int  getCode(){
        return this.status;
    }
}
