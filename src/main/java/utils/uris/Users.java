package utils.uris;

public enum Users {

    URI_GET_ALL_USERS  ("users"),
    URI_GET_USER_BY_ID("users/{id}"),
    URI_POST_USER("users"),
    URI_PUT_BY_ID("users/{id}"),
    URI_DELETE_BY_ID("users/{id}");


    private final String uri;

    private Users(String text) {
        this.uri = text;
    }

    public String  getURI(){
        return this.uri;
    }
}
