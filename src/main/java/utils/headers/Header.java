package utils.headers;

import java.util.HashMap;
import java.util.Map;

public class Header {

    private static Map<String, String> headers = new HashMap<String, String>();

    public static Map<String, String> getMainHeader() {
        headers.put("Content-Type", "application/json");
        return headers;
    }

    public static Map<String, String> getHeaderTransaction() {
        headers.put("Content-Type", "application/json");
        headers.put("value", "value");
        return headers;
    }

}
