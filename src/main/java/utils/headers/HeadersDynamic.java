package utils.headers;

import java.util.HashMap;
import java.util.Map;

public class HeadersDynamic {

    private static Map<String, String> headers = new HashMap<String, String>();

    public static Map<String, String> getHeaderTransaction(String value) {
        headers.put("Content-Type", "application/json");
        headers.put("value", value);
        return headers;
    }
}
