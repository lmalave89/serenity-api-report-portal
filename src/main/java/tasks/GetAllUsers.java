package tasks;

import exceptions.GetExceptions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;
import utils.StatusCode;
import utils.headers.Header;
import utils.uris.Users;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class GetAllUsers implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            actor.attemptsTo(Get.resource(Users.URI_GET_ALL_USERS.getURI())
                    .with(request->request.headers(Header.getMainHeader())));
            actor.should(seeThatResponse("Service Get all users executed success",
                    response -> response.statusCode(StatusCode.CODE_200.getCode())));
        }catch(Exception e){
            new GetExceptions(GetExceptions.FAILED_REFERENCE, e);
        }
    }

    public static GetAllUsers execute() {
        return Tasks.instrumented(GetAllUsers.class);
    }

}
