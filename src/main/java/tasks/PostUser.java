package tasks;

import com.google.gson.JsonObject;
import exceptions.PostExceptions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;
import utils.StatusCode;
import utils.bodies.PostUsers;
import utils.headers.Header;
import utils.uris.Users;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class PostUser implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            actor.attemptsTo(Post.to(Users.URI_POST_USER.getURI())
                    .with(request->request.headers(Header.getMainHeader()).body(PostUsers.getData())));
            actor.should(seeThatResponse("Service Post user executed success",
                    response -> response.statusCode(StatusCode.CODE_201.getCode())));
        }catch(Exception e){
            new PostExceptions(PostExceptions.FAILED_REFERENCE, e);
        }
    }

    public static PostUser execute() {
        return Tasks.instrumented(PostUser.class);
    }
}
