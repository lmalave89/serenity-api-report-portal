package tasks;

import exceptions.DeleteExceptions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import utils.StatusCode;
import utils.headers.Header;
import utils.uris.Users;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class DeleteUser implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            actor.attemptsTo(Delete.from(Users.URI_DELETE_BY_ID.getURI())
                    .with(request -> request.headers(Header.getMainHeader())
                            .pathParam("id", 11)));
            actor.should(seeThatResponse("Service Delete user by id executed success",
                    response -> response.statusCode(StatusCode.CODE_200.getCode())));
        } catch (Exception e) {
            new DeleteExceptions(DeleteExceptions.FAILED_REFERENCE, e);
        }
    }

    public static DeleteUser execute() {
        return Tasks.instrumented(DeleteUser.class);
    }

}
