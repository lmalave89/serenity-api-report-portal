package tasks;

import exceptions.PutExceptions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Put;
import utils.StatusCode;
import utils.bodies.PutUsers;
import utils.headers.Header;
import utils.uris.Users;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;


public class PutUser implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {

        try {
            actor.attemptsTo(Put.to(Users.URI_PUT_BY_ID.getURI())
                    .with(request->request.headers(Header.getMainHeader())
                            .pathParam("id", 11)
                    .body(PutUsers.getData())));
            actor.should(seeThatResponse("Service Put user executed success",
                    response -> response.statusCode(StatusCode.CODE_200.getCode())));
        }catch(Exception e){
            new PutExceptions(PutExceptions.FAILED_REFERENCE, e);
        }

    }

    public static PutUser execute() {
        return Tasks.instrumented(PutUser.class);
    }
}
