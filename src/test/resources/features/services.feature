Feature: Automation services Report Portal

  Background:
    Given Query all users of platform

  Scenario: CRUD user in platform
    And Get a user by id
    When Insert new user
    And Update user
    And Delete user
    Then verify user was delete
