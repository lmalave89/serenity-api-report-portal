package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tasks.*;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ServicesSteps extends BaseTest{

    public ServicesSteps(){}

    @Given("^Query all users of platform$")
    public void query_all_users_of_platform(){
        theActorInTheSpotlight().wasAbleTo(GetAllUsers.execute());
    }

    @When("^Insert new user$")
    public void insert_new_user(){
        theActorInTheSpotlight().attemptsTo(PostUser.execute());
    }

    @And("^Get a user by id$")
    public void get_a_user_by_id(){
        theActorInTheSpotlight().attemptsTo(GetUser.execute());
    }

    @And("^Update user$")
    public void update_user(){
        theActorInTheSpotlight().attemptsTo(PutUser.execute());
    }

    @And("^Delete user$")
    public void delete_user(){
        theActorInTheSpotlight().attemptsTo(DeleteUser.execute());
    }

    @Then("^verify user was delete$")
    public void verify_user_was_delete(){
        theActorInTheSpotlight().attemptsTo(GetAllUsers.execute());
    }
}
